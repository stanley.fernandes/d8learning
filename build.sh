#!/bin/bash
export HOME=/root
echo "drupal deployment"
ls
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
chmod +x /usr/local/bin/composer
wget -O drush.phar https://github.com/drush-ops/drush-launcher/releases/latest/download/drush.phar
chmod +x drush.phar
mv drush.phar /usr/local/bin/drush
aws deploy push --application-name drupal-deployment --s3-location s3://drupal-deployment-artifact/app.zip --source . 
aws deploy create-deployment --application-name drupal-deployment --s3-location bucket=drupal-deployment-artifact,key=app.zip,bundleType=zip --deployment-group-name d1-group --file-exists-behavior OVERWRITE 
